﻿using nSql.sqlite;
using nSql.tools;
using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace nSql
{
    public partial class SQLiteConfig : Window
    {
        public string dbFile { get; set; }
        public int versionFile { get; set; }
        public int avalibleVersionFile { get; set; }
        SqliteDb sqlite = new SqliteDb();

        public SQLiteConfig()
        {
            InitializeComponent();


            dbFile = Properties.Settings.Default.SQLite_db_file;
            versionFile = sqlite.getDBVersion(dbFile);
            avalibleVersionFile = sqlite.GetAvailableDBVersion();

            sqlite_grid.DataContext = this;
        }

        private void saveDBFileToSettings()
        {
            Properties.Settings.Default.SQLite_db_file = dbFile;
            Properties.Settings.Default.Save();
        }

        private void refreshStatus()
        {
            versionFile = sqlite.getDBVersion(dbFile);
            versionLabel.Content = versionFile;
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            refreshStatus();
            saveDBFileToSettings();
        }

        private void Button_Click_CreateDB(object sender, RoutedEventArgs e)
        {
            if (sqlite.CreateDb(dbFile) == 0)
            {
                saveDBFileToSettings();
            }
            refreshStatus();
        }

        private void Button_Click_ClearDB(object sender, RoutedEventArgs e)
        {
            sqlite.ClearDb(dbFile);
            refreshStatus();
        }
        
        private void Button_Click_Refresh_Status(object sender, RoutedEventArgs e)
        {
            refreshStatus();
        }

        private void Button_Click_Update(object sender, RoutedEventArgs e)
        {
            sqlite.UpdateDb(dbFile);
            refreshStatus();
        }

        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_File_Selection(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFD = new SaveFileDialog();
            saveFD.Filter = "Database files (*.db)|*.db|All files (*.*)|*.*";
            saveFD.DefaultExt = "db";
            saveFD.Title = "Select database or enter the name of the new database.";
            saveFD.CheckFileExists = false;
            saveFD.OverwritePrompt = false;

            String actualDirectory = null;
            String initialDirectory = null;
            try
            {
                actualDirectory = Path.GetDirectoryName(dbFile);
            }
            catch (Exception ex)
            {
                Messages.warning(ex.Message);
            }

            if (actualDirectory == null || actualDirectory == String.Empty || actualDirectory.Length == 0)
            {
                initialDirectory = System.Windows.Forms.Application.StartupPath;
            }
            else
            {
                initialDirectory = actualDirectory;
            }

            saveFD.InitialDirectory = initialDirectory;

            if (saveFD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                dbFile = saveFD.FileName;
                dbFileNameTextBox.Text = dbFile;
            }

            if(File.Exists(dbFile) == false)
            {
                if (sqlite.CreateDb(dbFile) == 0)
                {
                    saveDBFileToSettings();
                }
                refreshStatus();
            } else
            {
                refreshStatus();
                if(versionFile > 0)
                {
                    saveDBFileToSettings();
                }
            }
        }

    }
}

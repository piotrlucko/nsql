﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nSql.models
{
    public class SQLConnectModel
    {
        private long id = -1;
        private String serverName;
        private String databaseName;
        private String connectionName;
        private bool isWindowsAuthentication;
        private String user;
        private String password;

        public long Id { get => id; set => id = value; }
        public string ServerName { get => serverName; set => serverName = value; }
        public string DatabaseName { get => databaseName; set => databaseName = value; }
        public string ConnectionName { get => connectionName; set => connectionName = value; }
        public bool IsWindowsAuthentication { get => isWindowsAuthentication; set => isWindowsAuthentication = value; }
        public string User { get => user; set => user = value; }
        public string Password { get => password; set => password = value; }

        public override string ToString()
        {
            return "id: " + id +
                ", serverName: " + serverName +
                ", databaseName: " + databaseName +
                ", connectionName: " + connectionName +
                ", isWindowsAuthentication: " + isWindowsAuthentication +
                ", user: " + user +
                ", password: " + password;
    }

        public SQLConnectModel clone()
        {
            SQLConnectModel newSQLConnect = new SQLConnectModel();
            newSQLConnect.id = this.id;
            newSQLConnect.connectionName = this.connectionName;
            newSQLConnect.serverName = this.serverName;
            newSQLConnect.databaseName = this.databaseName;
            newSQLConnect.isWindowsAuthentication = this.isWindowsAuthentication;
            newSQLConnect.user = this.user;
            newSQLConnect.password = this.password;

            return newSQLConnect;
        }

        public String GetConnectToServerString()
        {
            String connectString = "";

            if (serverName != null)
            {
                connectString += "Server=" + serverName + ";"; 
            }
            if(isWindowsAuthentication == true)
            {
                connectString += "Trusted_Connection=Yes;";
            } else
            {
                connectString += "User Id=" + user + ";";
                connectString += "Password=" +  password + ";";
            }

            return connectString;
           
        }

        public String GetConnectToDBString()
        {
            String connectString = GetConnectToServerString();
            connectString += "Database =" + databaseName + ";";

            return connectString;

        }

        public String getConnectString()
        {
            if (databaseName != null && databaseName.CompareTo("") != 0)
            {
                return GetConnectToDBString();
            } else
            {
                return GetConnectToServerString();
            }
        }


    }
}

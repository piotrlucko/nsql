﻿using nSql.models;
using nSql.sqlite;
using nSql.tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace nSql.tabGrid
{
    /// <summary>
    /// Logika interakcji dla klasy UserControlSql.xaml
    /// </summary>
    public partial class UserControlSql : UserControl, INotifyPropertyChanged
    {
        private SQLConnectModel connectParameters;
        private List<SQLConnectModel> connectionsList;
        private SqliteSQLConnection sqliteSQLConnection;
        private long idConnectParameters = -69;
        private String dbFile;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    
    public UserControlSql()
        {
            InitializeComponent();

            connectParameters = new SQLConnectModel();
            sqliteSQLConnection = new SqliteSQLConnection();
            dbFile = Properties.Settings.Default.SQLite_db_file;
            connectionsList = sqliteSQLConnection.ReadAllDb(dbFile);

            sql_grid.DataContext = this;
        }

        public SQLConnectModel ConnectParameters { get => connectParameters; set => connectParameters = value; }
        public List<SQLConnectModel> ConnectionsList { get => connectionsList; set => connectionsList = value; }
        public long IdConnectParameters { get => idConnectParameters;
            set 
            {
                idConnectParameters = value;
                FindAndSetSellectedConnection(value);
            }
        }

        private void FindAndSetSellectedConnection(long id)
        {
            foreach(SQLConnectModel connection in connectionsList)
            {
                if(connection.Id == id)
                {
                    connectParameters = connection;
                    OnPropertyChanged("ConnectParameters");
                }
            }

        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            if(connectParameters.ConnectionName == null || connectParameters.ConnectionName.Equals(""))
            {
                Messages.warning("You must enter the name of the connection.");
            } else
            {
                connectParameters.Password = passwordBox.Password;
                sqliteSQLConnection.SaveDb(dbFile, connectParameters);
            }
        }
    }
}

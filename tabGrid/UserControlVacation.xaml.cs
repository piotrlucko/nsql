﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Net.Mail;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;

namespace nSql
{
    /// <summary>
    /// Logika interakcji dla klasy UserControlVaation.xaml
    /// </summary>
    public partial class UserControlVacation : UserControl
    {
        public UserControlVacation()
        {
            InitializeComponent();
        }

        private void Print_Button_Click(object sender, RoutedEventArgs e)
        {
            Grid grid = (Grid)this.FindName("grid_test");
            PrintDialog printDlg = new PrintDialog();

            if (printDlg.ShowDialog() == true)

            {

                printDlg.PrintVisual(grid, "printDlg");

            }

        }

        private void Save_PDF_Button_Click(object sender, RoutedEventArgs e)
        {
            // Create paragraph with some text.
            Paragraph myParagraph = new Paragraph();
            myParagraph.Inlines.Add(new Run("Some paragraph text."));

            // Create a FlowDocument and add the paragraph to it.
            FlowDocument myFlowDocument = new FlowDocument();
            myFlowDocument.Blocks.Add(myParagraph);

            PrintDialog pd = new PrintDialog();
            IDocumentPaginatorSource dps = myFlowDocument;
            pd.PrintDocument(dps.DocumentPaginator, "flow doc");

            //this.Content = myFlowDocument;
        }


        private void Preview_Button_Click(object sender, RoutedEventArgs e)
{

            FixedDocument document = CreateFixedDocument();

            PrintPreview preview = new PrintPreview();
            preview.Document = document;
            preview.ShowDialog();

            /*  try
              {


                  PrintDialog printDialog = new PrintDialog();
                  bool? pdResult = printDialog.ShowDialog();
                  if (pdResult != null && pdResult.Value)
                  {
                      FixedDocument document = CreateFixedDocument();
                      printDialog.PrintDocument(document.DocumentPaginator, "ID Card Printing");
                  }
                  MessageBox.Show("Printing done.");
              }
              catch (Exception ex)
              {
                  Debug.WriteLine(ex.Message + " :: " + ex.InnerException);
              }*/
        }



        private FixedDocument CreateFixedDocument()
        {
            double MARGIN = 0.5;
            double FONTSIZE = 20;

            //A4
            FixedPage page = new FixedPage();
            page.Width = 96 * 8.5;
            page.Height = 96 * 11.0;

            TextBlock tbName = new TextBlock();
            tbName.Text = "Piotr Łućko\n";
            tbName.FontSize = FONTSIZE;
            tbName.Inlines.Add(new Run("(dene pracownika)") { TextDecorations = TextDecorations.OverLine });

            FixedPage.SetLeft(tbName, 96 * MARGIN); // left margin
            FixedPage.SetTop(tbName, 96 * MARGIN); // top margin
            page.Children.Add((UIElement)tbName);

            TextBlock tbPlaceAndData = new TextBlock();
            tbPlaceAndData.Text = "Biała Podlaska, 03.10.2020r.\n";
            tbPlaceAndData.Inlines.Add(new Run("(miejscowość, data)") { TextDecorations = TextDecorations.OverLine });
            tbPlaceAndData.FontSize = FONTSIZE;

            FixedPage.SetRight(tbPlaceAndData, 96 * MARGIN); // left margin
            FixedPage.SetTop(tbPlaceAndData, 96 * MARGIN); // top margin
            page.Children.Add((UIElement)tbPlaceAndData);

            TextBlock tbApplication = new TextBlock();
            tbApplication.Text = "Wniosek o udzielenie urlopu";
            tbApplication.FontSize = FONTSIZE * 1.2;
            tbApplication.Width = page.Width;
            tbApplication.FontWeight = FontWeights.Bold;
            tbApplication.TextAlignment = TextAlignment.Center;

            FixedPage.SetTop(tbApplication, (96 * MARGIN) + (4 * FONTSIZE)); // top margin

            page.Children.Add((UIElement)tbApplication);

            page.UpdateLayout();

            FixedDocument fd = new FixedDocument();
            PageContent pageX = new PageContent();
            ((IAddChild)pageX).AddChild(page);
            fd.Pages.Add(pageX);

            return fd;

        }
    }
}

﻿using nSql.tools;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;

namespace nSql.sqlite
{
    class SqliteDb
    {
        private static readonly int DB_VERSION = 201905300;
        SQLiteConnection dbConnect;
        SQLiteCommand dbCommand;

        /*
         * return 0 - no error 
         * return != 0 - error
        */
        public int CreateDb(String fileDb)
        {
            int error = 0;
            if (File.Exists(fileDb))
            {
                String warringString = "Database not created. \n" +
                "File: \"" + fileDb + "\" exists.";

                Messages.warning(warringString);
                error = 1;
            }
            else
            {
                try
                {
                    dbConnect = new SQLiteConnection("Data Source =" + fileDb + ";Version=3;");
                    dbConnect.Open();
                    dbCommand = new SQLiteCommand(SqliteQueries.dbQuerryCreateDB(DB_VERSION), dbConnect);
                    dbCommand.ExecuteNonQuery();
                    Messages.information("Database created.");
                }
                catch (Exception sqle)
                {
                    String errorString = "Database not created. \n" +       
                        sqle.Message;

                    Messages.error(errorString);
                    error = 2;
                }
                finally
                {
                    dbConnect.Close();
                }
            }

            return error;
        }

        public void UpdateDb(String fileDb)
        {
            int dbActualVersion = getDBVersion(fileDb);
            if(dbActualVersion == 0)
            {
                string errorString = "File: \"" + fileDb + "\"\n" +
                    "is not a valid database.";
                Messages.error(errorString);
            } else if (dbActualVersion == DB_VERSION) {
                string information = "Database is current.";
                Messages.information(information);
            } else if (dbActualVersion < DB_VERSION)
            {
                //UPDATE
                string updateMessage = "";

                if (getDBVersion(fileDb) < 201905300)
                {
                    updateMessage += "update to: v.: 201905300 \n";
                    updateMessage += updateQuery(fileDb, SqliteQueries.createSQLConnectionTable, 201905300);

                }

                //...
 
                Messages.information(updateMessage);
            } else
            {
                string warning = "Your database comes from the future.";
                Messages.warning(warning);
            }
        }

        public void ClearDb(String fileDb)
        {
            int dbActualVersion = getDBVersion(fileDb);
            if (dbActualVersion == 0)
            {
                string errorString = "File: \"" + fileDb + "\"\n" +
                    "is not a valid database.";
                Messages.error(errorString);
            } else
            {
                string question = "Clear database?";
                if (Messages.questionYesNo(question) == 1)
                {
                    deleteAndCreateTable(fileDb);
                }
            }
        }

        private void deleteAndCreateTable(String fileDb)
        {
            try
            {
                dbConnect = new SQLiteConnection("Data Source =" + fileDb + ";Version=3;");
                dbConnect.Open();
                dbCommand = new SQLiteCommand(SqliteQueries.dbQuerryDeleteTables, dbConnect);
                dbCommand.ExecuteNonQuery();
                dbCommand = new SQLiteCommand(SqliteQueries.dbQuerryCreateDB(DB_VERSION), dbConnect);
                dbCommand.ExecuteNonQuery();
                Messages.information("Database clean.");
            }
            catch (SQLiteException sqle)
            {
                String errorString = "Database not created. \n" +
                    "error code: " + sqle.ErrorCode + "\n" +
                    sqle.Message;

                Messages.error(errorString);
            }
            finally
            {
                dbConnect.Close();
            }
        }

        public int GetAvailableDBVersion()
        {
            return DB_VERSION;
        }

        public int getDBVersion(String fileDb)
        {
            int version = 0;
            if (!File.Exists(fileDb))
            {
                version = 0;
            }
            else
            {
                try
                {
                    dbConnect = new SQLiteConnection("Data Source =" + fileDb + ";Version=3;");
                    dbConnect.Open();
                    dbCommand = new SQLiteCommand(SqliteQueries.dbQuerryGetVersion, dbConnect);
                    version = Convert.ToInt32(dbCommand.ExecuteScalar());
                }
                catch (SQLiteException sqle)
                {
                    version = 0;
                }
                finally
                {
                    dbConnect.Close();
                    dbConnect.Dispose();
                }
            }

            return version;
        }


        private string updateQuery(String fileDb, String query, int version)
        {
            String returnMessage = "";
            try
            {
                dbConnect = new SQLiteConnection("Data Source =" + fileDb + ";Version=3;");
                dbConnect.Open();
                dbCommand = new SQLiteCommand(query, dbConnect);
                dbCommand.ExecuteNonQuery();
                dbCommand = new SQLiteCommand(SqliteQueries.dbQuerrySetVersion(version), dbConnect);
                dbCommand.ExecuteNonQuery();
                returnMessage += "ok\n";
            }
            catch (Exception sqle)
            {
                String errorString = "Error update \n" +
                    sqle.Message;
                returnMessage += errorString + "\n";
            }
            finally
            {
                dbConnect.Close();
            }

            return returnMessage;
        }
    }
}

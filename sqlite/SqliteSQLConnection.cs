﻿using nSql.models;
using nSql.tools;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;

namespace nSql.sqlite
{
    class SqliteSQLConnection
    {
        SQLiteConnection dbConnect;
        SQLiteCommand dbCommand;

        public void SaveDb(String fileDb, SQLConnectModel sqlConnectOrg)
        {
            SQLConnectModel sqlConnect = sqlConnectOrg.clone();

            if (!File.Exists(fileDb))
            {
                String warringString = "Configure the sqlite database";

                Messages.warning(warringString);
            }
            else
            {
                try
                {
                    dbConnect = new SQLiteConnection("Data Source =" + fileDb + ";Version=3;");
                    dbConnect.Open();

                    //dbCommand = new SQLiteCommand(SqliteQueries.getIdByNameFromSQLConnection(sqlConnect.ConnectionName), dbConnect);
                    //SQLiteDataReader r = dbCommand.ExecuteReader();
                    //while (r.Read())
                    //{
                    //     sqlConnect.Id =  (long) r["id"];
                    //}

                    dbCommand = new SQLiteCommand(SqliteQueries.saveSQLConnection(sqlConnect), dbConnect);
                    dbCommand.ExecuteNonQuery();
                }
                catch (Exception sqle)
                {
                    String errorString = sqle.Message;

                    Messages.error(errorString);
                }
                finally
                {
                    dbConnect.Close();
                }
            }
        }

        public List<SQLConnectModel> ReadAllDb(String fileDb)
        {
            List<SQLConnectModel> connections = new List<SQLConnectModel>();

            if (!File.Exists(fileDb))
            {
                String warringString = "Configure the sqlite database";
                Messages.warning(warringString);

                return connections;
            }
            else
            {
                try
                {
                    dbConnect = new SQLiteConnection("Data Source =" + fileDb + ";Version=3;");
                    dbConnect.Open();

                    string stm = "SELECT * FROM sql_connection";

                    dbCommand = new SQLiteCommand(stm, dbConnect);
                    SQLiteDataReader rdr = dbCommand.ExecuteReader();

                    while (rdr.Read())
                    {
                        SQLConnectModel connection = new SQLConnectModel();

                        connection.Id = rdr.GetInt64(0);
                        connection.ConnectionName = rdr.GetString(1);
                        connection.ServerName = rdr.GetString(2);
                        connection.DatabaseName = rdr.GetString(3);
                        if(rdr.GetInt32(4) == 1)
                        {
                            connection.IsWindowsAuthentication = true;
                        } else
                        {
                            connection.IsWindowsAuthentication = false;
                        }
                        connection.User = rdr.GetString(5);
                        connection.Password = rdr.GetString(6);

                        connections.Add(connection);
                    }

                    return connections;
                }
                catch (Exception sqle)
                {
                    String errorString = sqle.Message;
                    Messages.error(errorString);

                    return connections;
                }
                finally
                {
                    dbConnect.Close();
                }

                return connections;
            }
        }

    }
}

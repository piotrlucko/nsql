﻿using nSql.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nSql.sqlite
{
    static class SqliteQueries
    {

        /*
         * CREATE + DROP TABLES 
         * 
         */
        public static string dbQuerryCreateDB(int DB_VERSION)
        {
            return createVersionTable(DB_VERSION) +
                createSQLConnectionTable;
        }

        public static string createVersionTable(int DB_VERSION) {
            string query = "CREATE TABLE IF NOT EXISTS version(id INTEGER, version INTEGER);" +
            "INSERT INTO version VALUES (0, " + DB_VERSION + ");\n";

            return query; 
        }

        public static string createSQLConnectionTable = "" +
            "CREATE TABLE IF NOT EXISTS sql_connection(" +
            "id INTEGER PRIMARY KEY," +
            "connection_name TEXT NOT NULL UNIQUE," +
            "server_name TEXT," +
            "database_name TEXT," +
            "windows_authentication INTEGER," +
            "user TEXT," +
            "password TEXT);\n";

        public static string dbQuerryDeleteTables = "" +
            "DROP TABLE IF EXISTS version;" +
            "DROP TABLE IF EXISTS sql_connection";

        /*
        * VERSION TABLE 
        * 
        */

        public static string dbQuerryGetVersion = "SELECT version from version WHERE id=0";

        public static string dbQuerrySetVersion(int version) {
            return "UPDATE version SET version = " + version + " where id=0;";
        }

        /*
         * SQL_CONNECTION TABLE
         * 
         */

        public static string saveSQLConnection(SQLConnectModel s)
        {
            String query = "";
            if (s.Id <= 0)
            {
                //brak rekordu w bazie - dodawanie
                query += "INSERT OR REPLACE INTO sql_connection" +
                    "(connection_name, server_name, database_name, windows_authentication, user, password)" +
                    " VALUES('"  + s.ConnectionName + "', '" + s.ServerName + "', '" + s.DatabaseName + "', ";

            } else
            {
                //rekord jest już w bazie - update
                query += "INSERT OR REPLACE INTO sql_connection" +
                "(id, connection_name, server_name, database_name, windows_authentication, user, password)" +
                " VALUES('" + s.Id + "', '" + s.ConnectionName + "', '" + s.ServerName + "', '" + s.DatabaseName + "', ";
            }

            if (s.IsWindowsAuthentication)
            {
                query += "1";
            }
            else
            {
                query += "0";
            }
            query += ", '" + s.User + "', '" + s.Password + "');";

            return query;
        }


        public static string getIdByNameFromSQLConnection(String name)
        {

            string query = "SELECT id FROM sql_connection WHERE connection_name = '" + name + "';";
            return query;
        }


    }
}

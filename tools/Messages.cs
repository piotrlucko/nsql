﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace nSql.tools
{
    class Messages
    {
        public static void information(string messages)
        {
            MessageBox.Show(messages, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static void warning(String messages)
        {
            MessageBox.Show(messages, "Waring", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public static void error(string messages)
        {
            MessageBox.Show(messages, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /* return
         * 1 - yes
         * 0 - no
         */
        public static int questionYesNo(string question)
        {
            MessageBoxResult answer = MessageBox.Show(question, "Question", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (answer == MessageBoxResult.Yes || answer == MessageBoxResult.OK) {
                return 1;
            } else
            {
                return 0;
            }
        }
    }
}
